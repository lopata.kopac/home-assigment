import { TestBed } from '@angular/core/testing';

import { ForecastService } from './forecast.service';
import { HttpClientModule } from '@angular/common/http';

describe('ForecastService', () => {
  let service: ForecastService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
    });
    service = TestBed.inject(ForecastService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
