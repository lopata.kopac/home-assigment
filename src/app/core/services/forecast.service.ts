import { Injectable, inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { WeatherResponse } from '../models/forecast.model';

const meteoUrl = 'https://api.open-meteo.com/v1/forecast?latitude=51.5085&longitude=-0.1257&hourly=temperature_2m,surface_pressure&forecast_days=1';

@Injectable({
  providedIn: 'root'
})
export class ForecastService {
  private http = inject(HttpClient)
  
  getForecasts(){
    return this.http.get<WeatherResponse>(meteoUrl);
  }
}
