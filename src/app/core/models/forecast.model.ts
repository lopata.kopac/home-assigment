export interface WeatherTable {
    time: string
    state: string
    temperature: string
    pressure: string
}

export interface WeatherResponse {
    latitude: number
    longitude: number
    generationtime_ms: number
    utc_offset_seconds: number
    timezone: string
    timezone_abbreviation: string
    elevation: number
    hourly_units: HourlyUnits
    hourly: Hourly
}

export interface HourlyUnits {
    time: string
    temperature_2m: string
    surface_pressure: string
}

export interface Hourly {
    time: string[]
    temperature_2m: number[]
    surface_pressure: number[]
}