import { createActionGroup, props } from '@ngrx/store';
import { WeatherResponse } from '../../models/forecast.model';

export const ForecastActions = createActionGroup({
  source: 'Forecast',
  events: {
    'Load Forecasts': props<{ filter: unknown }>(),
    'Load Forecasts Success': props<{ data: WeatherResponse }>(),
    'Load Forecasts Failure': props<{ error: unknown }>(),
  }
});

export const { loadForecasts, loadForecastsSuccess, loadForecastsFailure } = ForecastActions;
