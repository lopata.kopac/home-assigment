import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { ForecastService } from '../../services/forecast.service';
import { catchError, map, of, switchMap } from 'rxjs';
import { ForecastActions } from './forecast.actions';

@Injectable()
export class ForecastEffects {
  loadForecasts$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ForecastActions.loadForecasts),
      switchMap(() =>
        this.forecastService.getForecasts().pipe(
          map(forecasts => ForecastActions.loadForecastsSuccess({ data: forecasts })),
          catchError(error => of(ForecastActions.loadForecastsFailure({ error })))
        )
      )
    )
  );

  constructor(private actions$: Actions, private forecastService: ForecastService) {}
}
