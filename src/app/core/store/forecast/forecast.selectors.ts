import { createFeatureSelector, createSelector } from '@ngrx/store';
import { ForecastState } from './forecast.reducer';

export const selectForecastState = createFeatureSelector<ForecastState>('forecast');

export const selectForecasts = createSelector(
  selectForecastState,
  (state) => state.forecasts
);

export const selectForecastStatus = createSelector(
  selectForecastState,
  (state) => state.status
);

export const selectForecastError = createSelector(
  selectForecastState,
  (state) => state.error
);

export const selectForecastFilter = createSelector(
  selectForecastState,
  (state) => state.filter
);