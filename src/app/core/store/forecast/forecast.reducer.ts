import { createReducer, on } from '@ngrx/store';
import { ForecastActions } from './forecast.actions';
import { WeatherResponse } from '../../models/forecast.model';

export const forecastFeatureKey = 'forecast';

export interface ForecastState {
  status: string;
  forecasts?: WeatherResponse;
  error: unknown;
  filter: unknown;
}

export const initialState: ForecastState = {
  status: 'idle',
  forecasts: undefined,
  error: null,
  filter: null
};

export const forecastReducer = createReducer(
  initialState,
  on(ForecastActions.loadForecasts, (state, {filter}) => ({
    ...state,
    status: 'loading',
    filter
  })),
  on(ForecastActions.loadForecastsSuccess, (state, { data }) => ({
    ...state,
    status: 'loaded',
    forecasts: data
  })),
  on(ForecastActions.loadForecastsFailure, (state, { error }) => ({
    ...state,
    status: 'error',
    error
  }))
);