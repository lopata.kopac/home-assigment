import { ForecastState } from "./forecast/forecast.reducer";

export interface AppState {
    forecast: ForecastState;
}