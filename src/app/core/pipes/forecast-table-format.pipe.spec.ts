import { TestBed } from '@angular/core/testing';
import { ForecastTableFormatPipe } from './forecast-table-format.pipe';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('ForecastTableFormatPipe', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule], 
    });
  });
  
  it('create an instance', () => {
    const pipe = new ForecastTableFormatPipe();
    expect(pipe).toBeTruthy();
  });
});
