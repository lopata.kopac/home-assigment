import { Pipe, PipeTransform } from '@angular/core';
import { WeatherResponse, WeatherTable } from '../models/forecast.model';

@Pipe({
  name: 'forecastTableFormat'
})
export class ForecastTableFormatPipe implements PipeTransform {

  transform(value: WeatherResponse | undefined | null): WeatherTable[] {
    if(!value){
      return [];
    }
    
    const result: WeatherTable[] = [];
    const leght = value?.hourly?.time?.length | 0;

    for(let index = 0; index < leght; index++){
        result.push({
            time: value.hourly.time[index],
            state: 'London',
            temperature: `${value.hourly.temperature_2m[index].toString()} ${value.hourly_units.temperature_2m}`,
            pressure: `${value.hourly.surface_pressure[index].toString()} ${value.hourly_units.surface_pressure}`
        });
    }

    return result;
  }
}
