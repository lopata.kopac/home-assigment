import { TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { TabTableHeaderComponent } from './features/forecast/tab-table-header/tab-table-header.component';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { NgApexchartsModule } from 'ng-apexcharts';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { ForecastEffects } from './core/store/forecast/forecast.effects';
import { forecastReducer } from './core/store/forecast/forecast.reducer';

describe('AppComponent', () => {
  beforeEach(() => TestBed.configureTestingModule({
    declarations: [
      AppComponent,
      TabTableHeaderComponent,
    ],
    imports: [
      BrowserModule,
      AppRoutingModule,
      BrowserAnimationsModule,
      HttpClientModule,
      NgApexchartsModule,
      StoreModule.forRoot({ forecast: forecastReducer }, {}),
      EffectsModule.forRoot([ForecastEffects]),
    ],
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });
});
