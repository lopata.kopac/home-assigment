import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TabHeatCalcComponent } from './tab-heat-calc.component';
import { ForecastMatModule } from '../forecast-mat.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('TabHeatCalcComponent', () => {
  let component: TabHeatCalcComponent;
  let fixture: ComponentFixture<TabHeatCalcComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TabHeatCalcComponent],
      imports: [
        CommonModule, ForecastMatModule, FormsModule, ReactiveFormsModule, BrowserAnimationsModule],
    });
    fixture = TestBed.createComponent(TabHeatCalcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
