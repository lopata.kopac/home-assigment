// Step 1: Import the necessary modules for creating a form.
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-tab-heat-calc',
  templateUrl: './tab-heat-calc.component.html',
  styleUrls: ['./tab-heat-calc.component.scss']
})
export class TabHeatCalcComponent implements OnInit{
  heatIndexForm: FormGroup = this.formBuilder.group({
    temperature: ['', Validators.required],
    temperatureUnit: ['C', Validators.required],
    humidity: ['', Validators.required]
  });
  heatIndex: number | null= null;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.heatIndexForm.valueChanges.subscribe(() => {
     this.heatIndex = null;
    });
  }

  onSubmit() {
    if (this.heatIndexForm.valid) {
      const values = this.heatIndexForm.value;
      const temperature = values.temperatureUnit === 'F' ? values.temperature : values.temperature * 9 / 5 + 32;
      const humidity = values.humidity;

      console.log(temperature);
      if (temperature < 26.7) {
        this.heatIndexForm.get('temperature')?.setErrors({ belowThreshold: true });
        return;
      }
      
      const calcHeatIndex = this.calculateHeatIndex(temperature, humidity);
      this.heatIndex = values.temperatureUnit === 'F' ? calcHeatIndex : (calcHeatIndex - 32) * 5 / 9;
    }
  }
  
  calculateHeatIndex(temperature: number, humidity: number) {
    return-42.379 + 2.04901523 * temperature + 10.14333127 * humidity 
      - 0.22475541 * temperature * humidity - 0.00683783 * temperature * temperature 
      - 0.05481717 * humidity * humidity + 0.00122874 * temperature * temperature * humidity 
      + 0.00085282 * temperature * humidity * humidity - 0.00000199 * temperature * temperature * humidity * humidity;
  }
}