import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ForecastRoutingModule } from './forecast-routing.module';
import { ForecastComponent } from './forecast/forecast.component';
import { TabTableComponent } from './tab-table/tab-table.component';
import { TabChartComponent } from './tab-chart/tab-chart.component';
import { TabHeatCalcComponent } from './tab-heat-calc/tab-heat-calc.component';
import { ForecastTableFormatPipe } from 'src/app/core/pipes/forecast-table-format.pipe';
import { NgApexchartsModule } from 'ng-apexcharts';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ForecastMatModule } from './forecast-mat.module';

@NgModule({
  declarations: [
    ForecastComponent,
    TabChartComponent,
    TabTableComponent,
    TabHeatCalcComponent,
    ForecastTableFormatPipe
  ],
  imports: [
    CommonModule,
    ForecastRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgApexchartsModule,
    ForecastMatModule,
  ]
})
export class ForecastModule { }
