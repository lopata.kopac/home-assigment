import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TabChartComponent } from './tab-chart.component';
import { ForecastMatModule } from '../forecast-mat.module';
import { NgApexchartsModule } from 'ng-apexcharts';

describe('TabChartComponent', () => {
  let component: TabChartComponent;
  let fixture: ComponentFixture<TabChartComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TabChartComponent],
      imports: [ForecastMatModule, NgApexchartsModule],
    });
    fixture = TestBed.createComponent(TabChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
