import { Component, Input, OnChanges, ViewChild } from '@angular/core';
import { ApexAxisChartSeries, ApexChart, ApexTitleSubtitle, ApexXAxis, ChartComponent } from 'ng-apexcharts';
import { WeatherResponse } from 'src/app/core/models/forecast.model';

export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  title: ApexTitleSubtitle;
};

@Component({
  selector: 'app-tab-chart',
  templateUrl: './tab-chart.component.html',
  styleUrls: ['./tab-chart.component.scss']
})
export class TabChartComponent implements OnChanges{
  @ViewChild("chart", { static: false }) chartChild!: ChartComponent;
  series: ApexAxisChartSeries = [
    {
      name: "temperature",
      data: []
    }
  ];
  chart: ApexChart  = {
    height: 350,
    type: "bar"
  };
  xaxis: ApexXAxis = {
    categories: []
  }
  title: ApexTitleSubtitle = {
    text: "Forecast for the next 24 hours"
  };

  @Input() data: WeatherResponse | undefined | null;
  @Input() isLoading = false;
  @Input() error: any = null;

  ngOnChanges() {
    if(!this.xaxis?.categories || !this?.series){
      return;
    }

    this.xaxis.categories = this.data?.hourly?.time || [];
    this.series = [
      {
        name: "temperature",
        data: this.data?.hourly?.temperature_2m || []
      }
    ];
  }
}
