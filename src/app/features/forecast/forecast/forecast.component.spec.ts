import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ForecastComponent } from './forecast.component';
import { TabChartComponent } from '../tab-chart/tab-chart.component';
import { TabTableComponent } from '../tab-table/tab-table.component';
import { TabHeatCalcComponent } from '../tab-heat-calc/tab-heat-calc.component';
import { ForecastTableFormatPipe } from 'src/app/core/pipes/forecast-table-format.pipe';
import { CommonModule } from '@angular/common';
import { ForecastMatModule } from '../forecast-mat.module';
import { StoreModule } from '@ngrx/store';
import { forecastReducer } from 'src/app/core/store/forecast/forecast.reducer';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgApexchartsModule } from 'ng-apexcharts';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('ForecastComponent', () => {
  let component: ForecastComponent;
  let fixture: ComponentFixture<ForecastComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        ForecastComponent,
        TabChartComponent,
        TabTableComponent,
        TabHeatCalcComponent,
        ForecastTableFormatPipe,
      ],
      imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NgApexchartsModule,
        ForecastMatModule,
        BrowserAnimationsModule,
        StoreModule.forRoot({ forecast: forecastReducer }, {}),
      ]
    });
    fixture = TestBed.createComponent(ForecastComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
