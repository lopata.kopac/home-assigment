import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, map } from 'rxjs';
import { loadForecasts } from 'src/app/core/store/forecast/forecast.actions';
import { ForecastState } from 'src/app/core/store/forecast/forecast.reducer';
import { selectForecastStatus, selectForecasts } from 'src/app/core/store/forecast/forecast.selectors';
import { WeatherResponse } from 'src/app/core/models/forecast.model';

@Component({
  selector: 'app-main-page',
  templateUrl: './forecast.component.html',
  styleUrls: ['./forecast.component.scss']
})
export class ForecastComponent implements OnInit {
  data$: Observable<WeatherResponse | undefined>;
  isLoading$: Observable<boolean>;

  constructor(private store: Store<{ forecast: ForecastState }> ) {
    this.data$ = this.store.select(selectForecasts);
    this.isLoading$ = this.store.select(selectForecastStatus).pipe(
      map(status => status === 'loading')
    );
  }

  ngOnInit() {
    this.store.dispatch(loadForecasts({filter: null}));
  }
}
