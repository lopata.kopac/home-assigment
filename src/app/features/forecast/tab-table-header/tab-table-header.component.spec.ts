import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TabTableHeaderComponent } from './tab-table-header.component';
import { ForecastMatModule } from '../forecast-mat.module';

describe('TabTableHeaderComponent', () => {
  let component: TabTableHeaderComponent;
  let fixture: ComponentFixture<TabTableHeaderComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TabTableHeaderComponent],
      imports: [ForecastMatModule],
    });
    fixture = TestBed.createComponent(TabTableHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
