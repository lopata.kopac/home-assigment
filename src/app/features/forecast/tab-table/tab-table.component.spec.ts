import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TabTableComponent } from './tab-table.component';
import { ForecastMatModule } from '../forecast-mat.module';
import { ForecastTableFormatPipe } from 'src/app/core/pipes/forecast-table-format.pipe';
import { MatTableModule } from '@angular/material/table';

describe('TabTableComponent', () => {
  let component: TabTableComponent;
  let fixture: ComponentFixture<TabTableComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TabTableComponent, ForecastTableFormatPipe],
      imports: [ForecastMatModule, MatTableModule],
    });
    fixture = TestBed.createComponent(TabTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
