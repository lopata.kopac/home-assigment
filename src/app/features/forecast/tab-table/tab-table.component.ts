import { Component, Input } from '@angular/core';
import { WeatherResponse } from 'src/app/core/models/forecast.model';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

@Component({
  selector: 'app-tab-table',
  templateUrl: './tab-table.component.html',
  styleUrls: ['./tab-table.component.scss'],
})
export class TabTableComponent {
  @Input() data: WeatherResponse | undefined | null;
  @Input() isLoading = false;
  @Input() error: any = null;

  displayedColumns: string[] = ['time', 'state', 'temperature', 'pressure'];


  getData(){
    return JSON.stringify(this.data);
  }
}