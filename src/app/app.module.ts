import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TabTableHeaderComponent } from './features/forecast/tab-table-header/tab-table-header.component';
import { HttpClientModule, provideHttpClient } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { forecastReducer } from './core/store/forecast/forecast.reducer';
import { ForecastEffects } from './core/store/forecast/forecast.effects';
import { NgApexchartsModule } from 'ng-apexcharts';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


@NgModule({
  declarations: [
    AppComponent,
    TabTableHeaderComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    NgApexchartsModule,
    StoreModule.forRoot({ forecast: forecastReducer }, {}),
    EffectsModule.forRoot([ForecastEffects]),
  ],
  providers: [provideHttpClient()],
  bootstrap: [AppComponent]
})
export class AppModule { }
